var gulp = require('gulp'),
    cssnano = require('gulp-cssnano'),
    autoprefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    clean = require('gulp-clean'),
    notify = require("gulp-notify"),
    sass = require("gulp-sass"),
    cssnano = require('gulp-cssnano'),
    sass = require('gulp-sass'),
    livereload = require('gulp-livereload'),
    jade = require('gulp-jade');

//images
gulp.task('image', function() {
    return gulp.src('src/img/*/*.*')
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{ removeViewBox: false }],
            use: [pngquant()]
        }))
        .pipe(gulp.dest('dist/img'))
        .pipe(livereload());
});

//clean 
gulp.task('clean', function() {
    return gulp.src('dist', { read: false })
        .pipe(clean());
});

//css
gulp.task('sass', function() {
    return gulp.src('src/sass/style.sass')
        .pipe(sass())
        .pipe(autoprefixer("last 15 versions"))
        .pipe(cssnano())
        .pipe(gulp.dest('dist/css/'))
        .pipe(livereload());
});

//js
gulp.task('js', function() {
    return gulp.src('src/js/*.js')
        .pipe(gulp.dest('dist/js/'))
        .pipe(livereload());
});

//сторонние библиотеки
gulp.task('vendor', function() {
    return gulp.src('vendor/*/*.*')
        .pipe(gulp.dest('dist/'))
});

//final building
gulp.task('jade', function() {
    return gulp.src('src/*.jade')
        .pipe(jade({
            pretty: true
        }))
        .pipe(gulp.dest('dist/'))
        .pipe(livereload());
});

//final building
gulp.task('final', ['sass', 'image', 'js', 'vendor'], function() {
    return gulp.src('src/*.jade')
        .pipe(jade())
        .pipe(gulp.dest('dist/'))
        .pipe(livereload());
});


gulp.task('default', ['vendor'], function() {
    livereload.listen();

    gulp.watch("src/*.jade", ['jade']);
    gulp.watch("src/*/*/*.jade", ['jade']);
    gulp.watch("src/sass/*.sass", ['sass']);
    gulp.watch("src/sass/*/*.sass", ['sass']);
    gulp.watch("src/js/*.js", ['js']);
    gulp.watch("src/img/*/*.*", ['image']);
});
